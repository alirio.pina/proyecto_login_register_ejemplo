Este proyecto fue realizado como demo de automatización de una aplicación de prueba.

Es necesario tener presente que las dependencias utilizadas para este proyecto (Ubicadas en el archivo build.gradle) son:

- Appium con versión 6.0.0
- Serenity con versión 3.2.4
- Junit con versión 4.12 
- Assertj-core con versión 3.8.0

Se realizó un testing basico de registro e ingreso, donde podemos validar los datos que se registraron.

La división de las carpetas son:

- src/main/java/com.test = para el mapeo, tasks y questions, también es posible agregar interactions o utils si aplican.

- src/test/java/com.test = es donde se ubican los runners, setup y stepdefinitions.

- src/test/java/resources = se encuentran las features, la apk correspondiente y el archivo de configuración de serinity (Donde podemos ajustar appium).

En los ajustes de appium se encuentra el nombre por defecto que Android Studio (deviceName = "emulator-5554") utiliza y el servidor por defecto que utiliza el programa Appium (hub = "http://0.0.0.0:4723/wd/hub").