package com.test.task;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Hit;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.test.ui.ExampleOverview.*;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isClickable;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class ExampleTask implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(

                Click.on(REGISTER),
                Enter.keyValues("Nombre").into(NAME),
                Enter.keyValues("example@gmail.com").into(EMAIL),
                Enter.keyValues("12345678").into(RE_PASSWORD),
                Enter.keyValues("12345678").into(RE_CONFIRM_PASSWORD),

                Click.on(CONFIRM_REGISTER),

                Enter.keyValues("Nombre 2").into(NAME),
                Enter.keyValues("example1@gmail.com").into(EMAIL),
                Enter.keyValues("12345678").into(RE_PASSWORD),
                Enter.keyValues("12345678").into(RE_CONFIRM_PASSWORD),

                Click.on(BACK),

                Enter.keyValues("example@gmail.com").into(EMAIL_LOGIN),

                Enter.keyValues("12345678").into(PASSWORD_LOGIN),

                Click.on(CONFIRM_LOGIN)

        );
    }

    public static ExampleTask exampleTask() {return new ExampleTask();}

}
