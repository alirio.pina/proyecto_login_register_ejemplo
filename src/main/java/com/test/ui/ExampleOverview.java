package com.test.ui;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class ExampleOverview {

    public static final Target REGISTER = Target
            .the("register")
            .located(By.id("com.loginmodule.learning:id/textViewLinkRegister"));

    public static final Target NAME = Target
            .the("namen")
            .located(By.id("com.loginmodule.learning:id/textInputEditTextName"));

    public static final Target EMAIL = Target
            .the("Email")
            .located(By.id("com.loginmodule.learning:id/textInputEditTextEmail"));

    public static final Target RE_PASSWORD = Target
            .the("pass")
            .located(By.id("com.loginmodule.learning:id/textInputEditTextPassword"));

    public static final Target RE_CONFIRM_PASSWORD = Target
            .the("confirm pass")
            .located(By.id("com.loginmodule.learning:id/textInputEditTextConfirmPassword"));

    public static final Target BACK = Target
            .the("Back")
            .located(By.id("com.loginmodule.learning:id/appCompatTextViewLoginLink"));


    public static final Target CONFIRM_REGISTER = Target
            .the("confirm register")
            .located(By.id("com.loginmodule.learning:id/appCompatButtonRegister"));

    public static final Target EMAIL_LOGIN = Target
            .the("Example btn")
            .located(By.id("com.loginmodule.learning:id/textInputEditTextEmail"));

    public static final Target PASSWORD_LOGIN = Target
            .the("Example btn")
            .located(By.id("com.loginmodule.learning:id/textInputEditTextPassword"));

    public static final Target CONFIRM_LOGIN = Target
            .the("Example btn")
            .located(By.id("com.loginmodule.learning:id/appCompatButtonLogin"));



}

