package com.test.question;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.targets.TheTarget;
import net.serenitybdd.screenplay.targets.Target;


public class TheScreenShouldHas {

    public static final Target EMAIL_DATA = Target
            .the("Email data")
            .located(By.id("com.loginmodule.learning:id/textViewEmail"));

    public static Question<String> emailData(){
        return TheTarget.textOf(EMAIL_DATA);
    }

    private TheScreenShouldHas() {
    }

}
